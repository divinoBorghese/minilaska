#include <stdio.h>
#include <windows.h>

#define RIGHE 7
#define COLONNE 7
#define N_PEDINE 11


typedef struct {
    int colore;                     /* 0 nero, 1 bianco */
    int size;
    int nConquistate;
    int promossa;                        /* 0 se pedina normale, 1 se promossa */
    int x;                          /* coordinate sulla board */
    int y;
} pedina;




typedef struct {
    int colore;                     /* 0 nero, 1 bianco */
    int libera;                     /* 0 occupata , 1 libera */
    int colorePedina;               /* NERA O BIANCA */
    pedina pedina1;
} cella;


void initBoard(cella campo[][COLONNE]){                   /* inizializza le celle e le pedine */
    int i, j;
    for(i = 0; i < RIGHE;i++) {
        for(j = 0; j < COLONNE;j++)
            if((i + j) % 2 == 0) {
                campo[i][j].colore = 1;
                campo[i][j].libera = 1;
                campo[i][j].colorePedina = -1;
            } else {
                campo[i][j].colore = 0;
                campo[i][j].libera = 1;
            }
        }
    }


void printBoard(cella campo[][COLONNE]) {              /* stampa la board sul terminale */
    int i, j;
    for(i = 0; i < RIGHE; i++) {
        for(j = 0; j < COLONNE; j++) {
            if (campo[i][j].colore == 0) {
                if(!campo[i][j].libera) {
                    if(campo[i][j].colorePedina == 1) {
                        printf("\333W\333");
                    } else {
                        printf("\333B\333");
                    }
                } else {
                    printf("\333\333\333");
                }
            } else {
                if(!campo[i][j].libera) {
                    if(campo[i][j].colorePedina == 0) {
                        printf(" W ");
                    } else {
                        printf(" B ");
                    }
                } else {
                    printf("   ");
                }
            }
        }
        printf("\n");
    }
}

void initPedine(pedina pedine[], int colore, cella campo[][COLONNE]) {
    int i, j, k;
    for(i = 0; i < N_PEDINE; i++) {
        pedine[i].promossa = 0;
        pedine[i].nConquistate = 0;
        pedine[i].colore = colore;
        if(colore == 0) {
            for(j = 0; j < 3; j++) {
                for(k = 0; k < COLONNE; k++) {
                    if((j + k) % 2 == 0) {
                        campo[j][k].libera = 0;
                        campo[j][k].colorePedina = colore;
                        campo[j][k].pedina1=pedine[i];
                        pedine[i].x = j;
                        pedine[i].y = k;
                    }
                }
            }
        } else {
            for(j = RIGHE - 3; j < RIGHE; j++) {
                for(k = 0; k < COLONNE; k++) {
                    if((j + k) % 2 == 0) {
                        campo[j][k].libera = 0;
                        campo[j][k].colorePedina = colore;
                        campo[j][k].pedina1=pedine[i];
                        pedine[i].x = j;
                        pedine[i].y = k;
                    }
                }
            }
        }
    }
}

void checkMosse(pedina pedine[], cella campo[][COLONNE]){
    int i, j;
    for(i = 0; i < N_PEDINE; i++) {
        if(pedine[i].colore == 0) {
            if(campo[pedine[i].x-1][pedine[i].y-1].libera==0) {
                if(campo[pedine[i].x-2][pedine[i].y-2].libera==1){
                    pedine[i].x=pedine[i].x-2;
                    pedine[i].y=pedine[i].y-2;
                  /*  pedine[i].nConquistate=1;*/
                    if(campo[pedine[i].x-1][pedine[i].y-1].pedina1.x==pedine[i].x-1)
                    campo[pedine[i].x-1][pedine[i].y-1].libera=1;

                }
            }
            }
        }
}

int  checkMosse1(pedina pedine[], cella campo[][COLONNE]) {
    int i, j;
    for (i = 0; i < N_PEDINE; i++) {
        if (campo[pedine[i].x - 1][pedine[i].y - 1].pedina1.x == pedine[i].x - 1)
            return 1;
    }
    return 0;
}

int main() {
    cella campo[RIGHE][COLONNE];
    pedina bianche[N_PEDINE];
    pedina nere[N_PEDINE];

    system("cls");

    initBoard(campo);
    initPedine(bianche, 1, campo);
    initPedine(nere, 0, campo);
    printBoard(campo);

    return 0;
}